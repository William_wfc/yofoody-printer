import { deletePrinters, postPrinters, putPrinters } from 'assets/Tools'

export const removePrinters = function ({dispatch}, printers) {
  for (let i = 0, len = printers.length; i < len; i++) {
    deletePrinters(printers[i])
  }
  console.log(printers)
  dispatch('REMOVEprinters', printers)
}

export const postPrinter = function ({dispatch}, data) {
  let callback = postPrinters(data)
  dispatch('POSTprinters', callback.data)
}

export const putPrinter = function ({dispatch}, pid, data) {  // 更新打印机
  let callback = putPrinters(pid, data)
  dispatch('PUTprinter', pid, callback.data)
}
