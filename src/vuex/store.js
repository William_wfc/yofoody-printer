import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import { getDomain, getPrinters } from 'assets/Tools'

getDomain()

const state = {
  allPrinters: getPrinters().data,
  handlePrinter: 0,  // 0:主界面；1:新增打印机；2:编辑打印机
  modifying: {}  // 定义要编辑的打印机
}

const mutations = {
  REMOVEprinters (state, printers) {
    let allPrinters = state.allPrinters
    printers.forEach(function (item, i) {
      for (let j = 0; j < allPrinters.length; j++) {
        if (allPrinters[j].id === item) {
          allPrinters.splice(j, 1)
          break
        }
      }
    })
  },
  ADDprinter (state) { // 到新增打印机界面
    state.handlePrinter = 1
  },
  CANCELadd (state) { // 取消新增打印机
    state.handlePrinter = 0
    state.modifying = {}
  },
  POSTprinters (state, newPrinter) { // 新增打印机
    state.allPrinters.push(newPrinter)
    state.handlePrinter = 0
  },
  MODIFYprinter (state, data) {  // 讲点击选中数据添加到要待更新打印机里
    state.modifying = data
    state.handlePrinter = 2
  },
  PUTprinter (state, pid, data) {  // 更新打印机
    let allPrinters = state.allPrinters
    for (let i = 0, len = allPrinters.length; i < len; i++) {
      if (allPrinters[i].id === pid) {
        allPrinters.splice(i, 1, data)
        break
      }
    }
    state.handlePrinter = 0
    state.modifying = {}
  }
}

export default new Vuex.Store({
  state,
  mutations
})
