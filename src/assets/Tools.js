import $ from 'n-zepto'

// 设置常量URL
const URL = {
  api: '/api/v1/restaurants/',
  rid1: 1,
  rid3: 3,
  ws: 'ws://54.223.37.180:61614'
}
// 取 domain
function getDomain () {
  // 取 URL.domain 放入store中
  console.log('getDomain')
  $.ajax({
    type: 'get',
    url: '/static/server.json',
    async: false,
    success: function (callback) {
      URL.api = 'http://' + callback.server + URL.api
      console.log(URL.api)
    },
    error: function (err) {
      window.alert('请求domain出错')
      console.log(err)
    }
  })
}
// 取所有打印机
function getPrinters () {
  var result
  $.ajax({
    type: 'get',
    url: URL.api + URL.rid3 + '/printers',
    async: false,
    success: function (callback) {
      result = callback
    },
    error: function (err) {
      window.alert('取打印机出错')
      console.log(err)
    }
  })
  return result
}
// 新增打印机
function postPrinters (_data) {
  var result
  $.ajax({
    type: 'post',
    url: URL.api + URL.rid3 + '/printers',
    async: false,
    data: JSON.stringify(_data),
    contentType: 'application/json',
    success: function (callback) {
      result = callback
    },
    error: function (err) {
      window.alert('新增打印机出错')
      console.log(err)
    }
  })
  return result
}
// 修改打印机
function putPrinters (_pid, _data) {
  var result
  $.ajax({
    type: 'put',
    url: URL.api + URL.rid3 + '/printers/' + _pid,
    async: false,
    data: JSON.stringify(_data),
    contentType: 'application/json',
    success: function (callback) {
      result = callback
    },
    error: function (err) {
      window.alert('修改打印机出错')
      console.log(err)
    }
  })
  return result
}
// 删除打印机
function deletePrinters (_pid) {
  var result
  $.ajax({
    type: 'delete',
    url: URL.api + URL.rid3 + '/printers/' + _pid,
    async: false,
    success: function (callback) {
      result = callback
    },
    error: function (err) {
      window.alert('删除打印机出错')
      console.log(err)
    }
  })
  return result
}

export { getDomain, getPrinters, postPrinters, putPrinters, deletePrinters }
